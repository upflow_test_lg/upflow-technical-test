import { ErrorRequestHandler } from 'express';
import { getLogger } from '../../connectors/logger';
import _ from 'lodash';

export const logErrorHandler: ErrorRequestHandler = (err, req, _res, next) => {
    if (err) {
        getLogger().error(
            'Error when calling server',
            err,
            _.pick(req, ['origin', 'method', 'path', 'hostname', 'port'])
        );
        next(err);
    }
    next();
};
