import { UserModel } from '../../../models/User/UserModel';

export interface CommandInputPostPDF {
    url: string;
    user: UserModel;
}
