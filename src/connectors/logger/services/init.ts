import { createLogger, format, Logger, LoggerOptions, transports } from 'winston';
import { TransportConfigs, TransportType } from '../types';

export const init = (config: LoggerOptions, transportsConfig: TransportConfigs[]) => {
    config.format = format.combine(
        format.timestamp(),
        format.json(),
        format.metadata(),
        format.errors({ stack: true })
    );
    config.transports = transportsConfig.map((transportConfig) => {
        switch (transportConfig.type) {
            case TransportType.console:
                return new transports.Console(transportConfig.options);
            case TransportType.file:
                return new transports.File(transportConfig.options);
        }
    });
    (globalThis as any).winstonLogger = createLogger(config);
    ((globalThis as any).winstonLogger as Logger).on('error', (error) => {
        console.log(error);
    });
};
