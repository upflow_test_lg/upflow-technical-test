import { transports } from 'winston';

export enum TransportType {
    console = 'console',
    file = 'file'
}

interface ConsoleTransportConfig {
    type: TransportType.console;
    options?: transports.ConsoleTransportOptions;
}

interface FileTransportConfig {
    type: TransportType.file;
    options?: transports.FileTransportOptions;
}

export type TransportConfigs = ConsoleTransportConfig | FileTransportConfig;
