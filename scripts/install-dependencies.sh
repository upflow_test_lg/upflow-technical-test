apt-get update
apt-get install -y curl unzip
curl -fsSL https://deb.nodesource.com/setup_17.x | bash -
apt-get install -y nodejs
apt-get update
apt-get install -y wget apt-transport-https software-properties-common liblttng-ust0
wget -q https://github.com/PowerShell/PowerShell/releases/download/v7.1.5/powershell_7.1.5-1.ubuntu.20.04_amd64.deb
dpkg -i powershell_7.1.5-1.ubuntu.20.04_amd64.deb
apt-get update
apt-get install -f
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
apt-get install -y mysql-client-8.0
apt-get install -y ghostscript
apt-get install -y imagemagick
apt-get install -y graphicsmagick