import { ApplicationPort } from './ApplicationPort';

export const releasePort = () =>
    ApplicationPort.update(
        {
            process_id: null
        },
        {
            where: { process_id: process.pid }
        }
    );
