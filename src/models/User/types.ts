import { Optional } from 'sequelize';

export interface UserPDFAttributes {
    id: number;
    userId: number;
    url: string;
    pdfS3Key: string;
    thumbnailS3Key: string;
}

export type UserPDFCreationAttributes = Optional<UserPDFAttributes, 'id'>;

export interface UserAttributes {
    id: number;
    login: string;
}

export type UserCreationAttributes = Optional<UserAttributes, 'id'>;
