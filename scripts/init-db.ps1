param([switch] $verbose, [switch] $no_docker_check)
$json = Get-Content "./src/config/config.json" | ConvertFrom-Json -Depth 32
$environment = $json.$($Env:NODE_ENV ? $Env:NODE_ENV : 'local')

if ($environment.databases.sql.config.storage -ne ":memory:") {
    $Env:MYSQL_PWD = $environment.databases.sql.config.password 
    $Env:MYSQL_HOST = $environment.databases.sql.config.host 
    $Env:MYSQL_TCP_PORT = $environment.databases.sql.config.port
    $Env:MYSQL_USER = $environment.databases.sql.config.username
    if (!$no_docker_check) {
        $db_healthy = docker inspect --format "{{json .State.Health.Status }}" mysql | ConvertFrom-Json
        while ($db_healthy -ne "healthy") {
            Start-Sleep -Seconds 2
            $db_healthy = docker inspect --format "{{json .State.Health.Status }}" mysql | ConvertFrom-Json
        }
    }

    if ($verbose) {
        mysql --protocol=tcp --verbose -u $Env:MYSQL_USER -e "DROP SCHEMA IF EXISTS upflow"
        mysql --protocol=tcp --verbose -u $Env:MYSQL_USER -e "CREATE SCHEMA upflow"
    } else {
        mysql --protocol=tcp -u $Env:MYSQL_USER -e "DROP SCHEMA IF EXISTS upflow"
        mysql --protocol=tcp -u $Env:MYSQL_USER -e "CREATE SCHEMA upflow"
    }
}

$Env:MYSQL_PWD = $environment.databases.portManagementSql.config.password 
$Env:MYSQL_HOST = $environment.databases.portManagementSql.config.host 
$Env:MYSQL_TCP_PORT = $environment.databases.portManagementSql.config.port
$Env:MYSQL_USER = $environment.databases.portManagementSql.config.username

if ($verbose) {
    mysql --protocol=tcp --verbose -u $Env:MYSQL_USER -e "DROP SCHEMA IF EXISTS PortManagement"
    mysql --protocol=tcp --verbose -u $Env:MYSQL_USER -e "CREATE SCHEMA PortManagement"
} else {
    mysql --protocol=tcp -u $Env:MYSQL_USER -e "DROP SCHEMA IF EXISTS PortManagement"
    mysql --protocol=tcp -u $Env:MYSQL_USER -e "CREATE SCHEMA PortManagement"
}
