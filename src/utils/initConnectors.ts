import * as S3 from '../connectors/storage/s3';
import * as sql from '../connectors/databases/sql';
import * as winstonLogger from '../connectors/logger';
import { getConfigFromEnv } from './environment';
import { shutdownConnectors } from './shutdownConnectors';

export const initConnectors = () => {
    if (!(globalThis as any).isInitialized) {
        const { databases, logger, storage } = getConfigFromEnv();
        sql.init(databases.sql.config);
        winstonLogger.init(logger.config, logger.transports);
        S3.init(storage.s3.config, storage.s3.containers);
        (globalThis as any).isShutdown = false;
        (globalThis as any).isInitialized = true;
        winstonLogger.getLogger().info('Connectors initialized for SQL DB, S3 storage and logger');
        process.once('SIGINT', shutdownConnectors);
    }
};
