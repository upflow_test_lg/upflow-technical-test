import * as uuid from 'uuid';
import axios from 'axios';
import bluebird from 'bluebird';
import { fromBuffer } from 'pdf2pic';
import { CommandInputPostPDF } from '../types';
import { ObjectNotPDFError, ThumbnailCreationError, ObjectNotDownloadedError } from '../errors';
import {
    uploadPDFFileToS3,
    uploadThumbnailFileToS3,
    getUrlForPDFFile,
    getUrlForThumbnailFile
} from '../../../../connectors/storage/s3';
import { UserPDFModel } from '../../../../models/User/UserModel';
import { sendErrorUpload, sendSuccessfullUpload } from '../../../../websockets';
import { ToBase64Response } from 'pdf2pic/dist/types/toBase64Response';
import { getConfigFromEnv } from '../../../../utils/environment';

const createThumbnail = async (userId: number, url: string, data: Buffer) => {
    const convert = fromBuffer(data, { format: 'png', quality: 800, width: 200, height: 300 });
    const setGMClass = convert.setGMClass;
    const { gmtool } = getConfigFromEnv();
    if (setGMClass) setGMClass(gmtool);
    const base64response = (await convert(1, true)) as ToBase64Response;
    if (!base64response.base64) {
        throw new ThumbnailCreationError(userId, url, gmtool);
    }
    return Buffer.from(base64response.base64, 'base64');
};

export const uploadPDFCommand = async (input: CommandInputPostPDF) => {
    const { url, user } = input;
    const { id: userId } = user;
    const pdfS3Key = uuid.v4() + '.pdf';
    const thumbnailS3Key = uuid.v4() + '.png';
    const [{ data, status, headers }] = await bluebird.Promise.all([
        axios.get(url, { responseType: 'arraybuffer' }),
        UserPDFModel.create({ userId, url, pdfS3Key, thumbnailS3Key })
    ]);
    if (status !== 200) {
        throw new ObjectNotDownloadedError(userId, url);
    }
    const contentType = headers['content-type'];
    if (contentType !== 'application/pdf') {
        throw new ObjectNotPDFError(userId, url, contentType);
    }
    return bluebird.Promise.all([
        pdfS3Key,
        thumbnailS3Key,
        uploadPDFFileToS3({ Key: pdfS3Key, Body: data }),
        createThumbnail(userId, url, data).then((thumbnail) =>
            uploadThumbnailFileToS3({ Key: thumbnailS3Key, Body: thumbnail })
        )
    ]);
};

export const uploadPDFCommandAndNotifyUser = async (input: CommandInputPostPDF) => {
    try {
        const [pdfS3Key, thumbnailS3Key] = await uploadPDFCommand(input);
        sendSuccessfullUpload(
            input.user.id,
            await getUrlForPDFFile(pdfS3Key),
            await getUrlForThumbnailFile(thumbnailS3Key)
        );
    } catch (error) {
        sendErrorUpload(input.user.id, error);
    }
};
