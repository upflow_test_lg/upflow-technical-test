import { Logger } from 'winston';

export const getLogger = () => {
    return (globalThis as any).winstonLogger as Logger;
};
