import { Column, DataType, Model, PrimaryKey, Table, Unique } from 'sequelize-typescript';
import { Optional } from 'sequelize/types';

interface ApplicationPortAttributes {
    port: number;
    process_id: number | null;
}

type ApplicationPortCreationAttributes = Optional<ApplicationPortAttributes, 'process_id'>;

@Table
export class ApplicationPort extends Model<ApplicationPortAttributes, ApplicationPortCreationAttributes> {
    @PrimaryKey
    @Column(DataType.INTEGER.UNSIGNED)
    port!: number;

    @Unique
    @Column(DataType.INTEGER.UNSIGNED)
    process_id!: number;
}
