import * as sql from '../connectors/databases/sql';
import * as S3 from '../connectors/storage/s3';
import * as winstonLogger from '../connectors/logger';
import io from 'socket.io';

export const shutdownConnectors = async () => {
    if (!(globalThis as any).isShutdown) {
        winstonLogger.getLogger().info('Shutdown connectors for SQL DB, s3 storage and logger');
        sql.shutdown();
        winstonLogger.shutdown();
        S3.shutdown();
        process.removeListener('SIGINT', shutdownConnectors);
        (globalThis as any).isShutdown = true;
        ((globalThis as any).wsNotificationUploadImage as io.Server).close();
    }
};
