import { S3 } from '@aws-sdk/client-s3';

export const shutdown = () => ((globalThis as any).s3Client as S3)?.destroy();
