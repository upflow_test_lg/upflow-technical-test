export interface RequestInputPostPDF {
    url: string;
}

export interface PDFDetail {
    pdfS3Location: string;
    thumbnailS3Location: string;
    url: string;
}

export interface RequestOutputGetPDF {
    pdfs: PDFDetail[];
}
