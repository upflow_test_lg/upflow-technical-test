import { APIError } from '../../../utils/commonErrors';

export class UserNotFoundError extends APIError {
    constructor(public readonly userId: number) {
        super('User not found');
        this.status = 404;
    }
}
