import { RequestHandler, Router } from 'express';
import { JTDSchemaType } from 'ajv/dist/jtd';

export interface RouteHandler<InputType = never, OutputType = never> {
    method: 'all' | 'get' | 'post' | 'put' | 'delete' | 'patch' | 'options' | 'head';
    path: string;
    inputSchema?: JTDSchemaType<InputType>;
    outputSchema?: JTDSchemaType<OutputType>;
    injectedHandlers?: RequestHandler[];
    routeHandler: RequestHandler;
}

export interface Controller {
    router: Router;
}
