import * as http from 'http';
import io from 'socket.io';
import { getLogger } from '../../connectors/logger';

export const initWSNotificationUploadImage = (server: http.Server) => {
    (globalThis as any).wsNotificationUploadImage = new io.Server(server, {
        path: '/websockets'
    });
    const wss = (globalThis as any).wsNotificationUploadImage as io.Server;
    wss.on('connection', (socket) => {
        const userId = socket.handshake.query['userId'];
        getLogger().info('User is connected to a room', { userId });
        if (userId) socket.join(userId);
    });
};
