import express from 'express';
import { initConnectors } from './utils/initConnectors';
import { runApplication } from './runApplication';

initConnectors();
runApplication(Number(process.env['PORT']) || 8080, express());
