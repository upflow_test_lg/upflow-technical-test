import _ from 'lodash';
import bluebird from 'bluebird';
import { getUrlForPDFFile, getUrlForThumbnailFile } from '../../../../connectors/storage/s3';
import { UserPDFModel } from '../../../../models/User/UserModel';

export const getListOfPDF = async (userId: number) => {
    const pdfs = await UserPDFModel.findAll({
        where: {
            userId
        }
    });
    const results = _.map(pdfs, (userPdf) => _.pick(userPdf, ['url', 'pdfS3Key', 'thumbnailS3Key']));
    return bluebird.Promise.map(results, async (result) => {
        const { url, pdfS3Key, thumbnailS3Key } = result;
        const [pdfS3Location, thumbnailS3Location] = await bluebird.Promise.all([
            getUrlForPDFFile(pdfS3Key),
            getUrlForThumbnailFile(thumbnailS3Key)
        ]);
        return { url, pdfS3Location, thumbnailS3Location };
    });
};
