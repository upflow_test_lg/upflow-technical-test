import { Sequelize, SequelizeOptions } from 'sequelize-typescript';
import { UserModel, UserPDFModel } from '../../../../models/User/UserModel';

export const init = (config: SequelizeOptions) => {
    (globalThis as any).sequelize = new Sequelize(config);
    const sequelize = (globalThis as any).sequelize as Sequelize;
    sequelize.addModels([UserModel, UserPDFModel]);
};
