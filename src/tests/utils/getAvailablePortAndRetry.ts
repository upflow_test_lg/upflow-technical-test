import * as crypto from 'crypto';
import retry from 'bluebird-retry';
import { APIError } from '../../utils/commonErrors';
import { ApplicationPort } from './ApplicationPort';
import { Sequelize } from 'sequelize-typescript';

class NoFreeAvailablePortsError extends APIError {
    public readonly process_id: number = process.pid;
    constructor() {
        super('No ports are available at the moment');
    }
}

class PortAlreadyTakenError extends APIError {
    public readonly process_id: number = process.pid;
    constructor(public readonly port: number) {
        super('No ports are available at the moment');
    }
}

const getAvailablePort = async () =>
    ((globalThis as any).sequelizePortManagement as Sequelize).transaction(async (transaction) => {
        const freeAvailablePorts = await ApplicationPort.findAll({
            where: { process_id: null },
            transaction
        });
        if (!freeAvailablePorts) {
            throw new NoFreeAvailablePortsError();
        }
        const { port } = freeAvailablePorts[crypto.randomInt(freeAvailablePorts.length)];
        const [affectedRows] = await ApplicationPort.update(
            {
                process_id: process.pid
            },
            { where: { port, process_id: null }, transaction }
        );
        if (!affectedRows) {
            throw new PortAlreadyTakenError(port);
        }
        return port;
    });

export const getAvailablePortAndRetry = () =>
    retry<number>(getAvailablePort, {
        max_tries: 10,
        predicate: (error: any) =>
            error.name === 'SequelizeUniqueConstraintError' || error.name === 'PortAlreadyTakenError'
    });
