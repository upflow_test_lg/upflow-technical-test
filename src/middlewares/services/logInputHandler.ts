import { RequestHandler } from 'express';
import { getLogger } from '../../connectors/logger';
import _ from 'lodash';

export const logInputHandler: RequestHandler = (req, _res, next) => {
    getLogger().info('Request coming', _.pick(req, ['origin', 'method', 'path', 'hostname', 'port']));
    next();
};
