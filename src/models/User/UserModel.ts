import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Unique
} from 'sequelize-typescript';
import { UserAttributes, UserCreationAttributes, UserPDFAttributes, UserPDFCreationAttributes } from './types';
/* eslint-disable no-use-before-define */

@Table
export class UserModel extends Model<UserAttributes, UserCreationAttributes> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER.UNSIGNED)
    id!: number;

    @AllowNull(false)
    @Column(DataType.STRING(256))
    login!: string;

    @HasMany(() => UserPDFModel)
    pdfs!: UserPDFModel[];
}

@Table({ indexes: [{ name: 'user_url_index', fields: ['userId', 'url'], unique: true }] })
export class UserPDFModel extends Model<UserPDFAttributes, UserPDFCreationAttributes> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER.UNSIGNED)
    id!: number;

    @ForeignKey(() => UserModel)
    @AllowNull(false)
    @Column(DataType.INTEGER.UNSIGNED)
    userId!: number;

    @AllowNull(false)
    @Column(DataType.STRING(256))
    url!: string;

    @AllowNull(false)
    @Unique
    @Column(DataType.STRING(256))
    pdfS3Key!: string;

    @AllowNull(false)
    @Unique
    @Column(DataType.STRING(256))
    thumbnailS3Key!: string;
}
