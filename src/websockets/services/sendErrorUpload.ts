import io from 'socket.io';
import { getLogger } from '../../connectors/logger';

export const sendErrorUpload = (userId: number, error: any) => {
    getLogger().error('Notify Room With Error In Upload', error, { userId });
    ((globalThis as any).wsNotificationUploadImage as io.Server).in(String(userId)).emit('Error Upload', error);
};
