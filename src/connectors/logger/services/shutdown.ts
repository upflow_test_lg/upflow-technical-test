import { Logger } from 'winston';

export const shutdown = () => {
    ((globalThis as any).winstonLogger as Logger)?.close();
};
