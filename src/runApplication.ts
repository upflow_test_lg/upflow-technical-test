import express from 'express';
import listOfControllers from './controllers';
import { getLogger } from './connectors/logger';
import { initWSNotificationUploadImage } from './websockets';
import { interceptResponseAndLog, logErrorHandler, logInputHandler, sendErrorStatusHandler } from './middlewares';

export const runApplication = (port: number, app: express.Express) => {
    app.use(express.json());

    app.use(logInputHandler);

    app.use(interceptResponseAndLog);

    listOfControllers.forEach((controller) => app.use('/', controller.router));

    app.use(logErrorHandler);

    app.use(sendErrorStatusHandler);

    const server = app.listen(port, () => {
        getLogger().info(`Server started at http://localhost:${port}`);
    });

    process.on('SIGINT', () => {
        console.info('SIGINT signal received: closing HTTP server');
        server.close(() => {
            console.info('HTTP server closed');
        });
    });

    initWSNotificationUploadImage(server);

    return server;
};
