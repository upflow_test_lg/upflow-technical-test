import { S3 } from '@aws-sdk/client-s3';
import { S3ClientNotFoundError, S3URLBucketNameNotFoundError, S3URLMapNotFoundError } from './errors';

export const uploadFileInBucket = async (params: { Key: string; Body: Buffer; bucketname: string }) => {
    const { Key, Body, bucketname } = params;
    const s3Client = (globalThis as any).s3Client as S3;
    if (!s3Client) {
        throw new S3ClientNotFoundError();
    }
    const containers = (globalThis as any).s3Containers as Map<string, string>;
    if (!containers) {
        throw new S3URLMapNotFoundError();
    }
    const Bucket = containers.get(bucketname);
    if (!Bucket) {
        throw new S3URLBucketNameNotFoundError(bucketname);
    }
    return s3Client.putObject({
        Bucket,
        Key,
        Body
    });
};

export const getUrlToGetObject = async (id_object: string, bucket_alias: string) => {
    const s3Client = (globalThis as any).s3Client as S3;
    if (!s3Client) {
        throw new S3ClientNotFoundError();
    }
    const { hostname, port, protocol } = await s3Client.config.endpoint();
    const containers = (globalThis as any).s3Containers as Map<string, string>;
    if (!containers) {
        throw new S3URLMapNotFoundError();
    }
    const bucketname = containers.get(bucket_alias);
    if (!bucketname) {
        throw new S3URLBucketNameNotFoundError(bucket_alias);
    }
    return s3Client.config.forcePathStyle
        ? `${protocol}//${hostname}:${port}/${bucketname}/${id_object}`
        : `https://${bucketname}.s3.amazonaws.com/${id_object}`;
};
