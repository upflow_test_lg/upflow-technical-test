import { APIError } from '../../../utils/commonErrors';

export class S3ClientNotFoundError extends APIError {
    constructor() {
        super('S3Client not found');
    }
}

export class S3URLBucketNameNotFoundError extends APIError {
    constructor(public readonly bucket_alias: string) {
        super('S3 Bucket name not found');
    }
}

export class S3URLMapNotFoundError extends APIError {
    constructor() {
        super('S3 map not found');
    }
}
