import ajvParser from '../utils/jsonValidator';
import { ErrorObject, JTDSchemaType } from 'ajv/dist/jtd';
import { findUser } from '../queries/Users/PDF';
import { JSONValidationError } from '../utils/commonErrors';
import { RequestHandler, Router } from 'express';
import { RouteHandler } from './types';

const defaultInputValidationHandler = <T>(schema: JTDSchemaType<T>) => {
    return async (req: any, _res: any, next: any) => {
        const validator = ajvParser.compile(schema);
        if (!validator(req.body)) {
            next(new JSONValidationError(validator.errors as ErrorObject[], schema));
        }
        next();
    };
};

const defaultOutputValidationHandler = <T>(schema: JTDSchemaType<T>) => {
    return async (_req: any, res: any, next: any) => {
        const { output } = res;
        const validator = ajvParser.compile(schema);
        if (!validator(output)) {
            next(new JSONValidationError(validator.errors as ErrorObject[], schema));
        }
        res.status(200).send(output);
    };
};

export const getRouterFromHandler = <InputType, OutputType>(
    router: Router,
    handler: RouteHandler<InputType, OutputType>
): Router => {
    const { method, path, inputSchema, outputSchema, routeHandler, injectedHandlers } = handler;
    const handlers = [
        inputSchema ? defaultInputValidationHandler(inputSchema) : undefined,
        ...(injectedHandlers || []),
        routeHandler,
        outputSchema ? defaultOutputValidationHandler(outputSchema) : undefined
    ].filter((currentHandler) => currentHandler);
    // eslint-disable-next-line security/detect-object-injection
    return (router as any)[method](path, ...handlers);
};

export const injectUserHandler: RequestHandler = async (req, _res, next) => {
    const userId = parseInt(req.params['userId']);
    try {
        const user = await findUser(userId);
        (req as any)['injected'] = { user };
    } catch (error) {
        next(error);
    }
    next();
};
