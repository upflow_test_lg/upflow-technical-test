import { APIError } from '../../../utils/commonErrors';

export class ObjectNotDownloadedError extends APIError {
    constructor(public readonly userId: number, public readonly url: string) {
        super('Object not found at this URL');
    }
}

export class ObjectNotPDFError extends APIError {
    constructor(public readonly userId: number, public readonly url: string, public readonly contentType: string) {
        super('Object at this URL is not a PDF');
    }
}

export class ThumbnailCreationError extends APIError {
    constructor(public readonly userId: number, public readonly url: string, public readonly gmtool: boolean) {
        super('Failed to create thumbnail from PDF');
    }
}
