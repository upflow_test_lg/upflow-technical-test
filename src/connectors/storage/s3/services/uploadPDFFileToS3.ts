import { File } from '../types';
import { uploadFileInBucket } from '../utils';

export const uploadPDFFileToS3 = (params: File) =>
    uploadFileInBucket({
        ...params,
        bucketname: 'UserPDF'
    });
