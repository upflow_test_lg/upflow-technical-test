import { ErrorObject, JTDSchemaType } from 'ajv/dist/jtd';

export class APIError extends Error {
    public status?: number;
    constructor(message: string) {
        super(message);
        this.name = new.target.name;
        Error.captureStackTrace(this);
    }
}

export class JSONValidationError<T> extends APIError {
    constructor(public readonly errorMessages: ErrorObject[], public readonly schema: JTDSchemaType<T>) {
        super('JSON validation error');
        this.status = 400;
    }
}
