import { getLogger } from '../../connectors/logger';
import { Sequelize } from 'sequelize-typescript';

export const shutdownPortManagement = async () => {
    getLogger().info('Shutdown connectors for port management');
    process.removeListener('SIGINT', shutdownPortManagement);
    return ((globalThis as any).sequelizePortManagement as Sequelize)?.close();
};
