import { ApplicationPort } from './ApplicationPort';
import { getConfigFromEnv } from '../../utils/environment';
import { getLogger } from '../../connectors/logger';
import { Sequelize } from 'sequelize-typescript';
import { shutdownPortManagement } from './shutdownPortManagement';

export const initPortManagement = () => {
    const { databases } = getConfigFromEnv();
    (globalThis as any).sequelizePortManagement = new Sequelize(databases.portManagementSql.config);
    (globalThis as any).sequelizePortManagement.addModels([ApplicationPort]);
    getLogger().info('Port management connector initialized');
    process.once('SIGINT', shutdownPortManagement);
};
