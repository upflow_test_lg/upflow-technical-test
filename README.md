# upflow_test

Upflow technical test

## Table of Contents

- [upflow_test](#upflow_test)
  - [Table of Contents](#table-of-contents)
  - [About](#about)
  - [Developing](#developing)
    - [Requirements](#requirements)
    - [Initial setup](#initial-setup)
    - [Build](#build)
    - [Test](#test)
    - [Linter](#linter)
    - [Dependencies](#dependencies)

## About

This repository solves the upflow technical exercise described [here](https://gist.github.com/barnaby/6ba4156b3fa46872765005452cc29bdb)

## Developing

### Requirements

The code has been tested on Windows 10 with Image Magick and on docker image ubuntu:latest with Graphics Magicks

- [nodeJS](https://www.nodejs.org) v17.x, our runtime for the API and tests
- [npm](https://www.npmjs.com) v8.x, our package manager
- [Docker](https://www.docker.com) and [Docker Compose](https://docs.docker.com/compose) to set up external services to run the server locally and developing
  - localstack for s3 (cloud storage)
  - MySQL (DB)
- [MySQL Client](https://dev.mysql.com/doc/refman/8.0/en/programs-client.html) v8.x used to set up MySQL DB locally and in the CI
- [Powershell](https://docs.microsoft.com/fr-fr/powershell/) v7.x used to set up env locally and in the CI (MySQL and localstack for s3)
- [AWS CLI](https://aws.amazon.com/fr/cli/) used to set up s3 locally and in the CI
- [Git](https://git-scm.com/) Code versioning
- [Graphics Magicks](http://www.graphicsmagick.org/) Binaries to convert images used by [pdf2pic](https://www.npmjs.com/package/pdf2pic) npm package
- [Image Magick](https://imagemagick.org/) Binaries to convert images used by [pdf2pic](https://www.npmjs.com/package/pdf2pic) npm package
- [Ghostscript](https://www.ghostscript.com/) Binaries to interpret PDF files used by [pdf2pic](https://www.npmjs.com/package/pdf2pic) npm package

If you are on Ubuntu or another linux distro with apt you can run `bash ./scripts/install-dependencies.sh` to install all the requirements for this repository\
You can then chose between Image Magick and Graphics Magick to create thumbnails if one or the other doesn't work by setting `gmtool`` boolean (true for IM false for GM) in [config.json](./src/config/config.json)

### Initial setup

1. Start the environment (docker containers, mysql DB and s3 storage) with `npm run env:init` description in [docker-compose.yml](./docker-compose.yml)
2. Install npm dependencies with `npm install`
3. Build Project see [build section](#build)
4. Start the server with `npm run start`. You have 2 users by default (id 1 and 2)

### Build

- Build all the sources from scratch with `npm run build` that use tsc typescript compiler. The version of typescript used can be found in [package.json](./package.json)
- Build incrementally with `npm run build:ts`
- Copy config changes with `npm run copy:configs`
- Clean build folder with `npm run build:clean`
- Build from scratch in watch mode with `npm run watch`
- Build incrementally in watch mode with `npm run watch:ts`

### Test

We used mocha as our tests runner, mochawesome and junit for reporting, nyc for code coverage

- Run all tests suite using js built sources `npm run test`

### Linter

We used eslint with prettier plugin. You can see rules in [.eslintrc.json](./.eslintrc.json)

### Dependencies

Code dependencies are checked using madge

- Check circular dependencies with [server.ts](./src/server.ts) as entrypoint with `npm run check:circular-dependencies`
