param([switch] $verbose)
$json = Get-Content "./src/config/config.json" | ConvertFrom-Json -Depth 32
$environment = $json.$($Env:NODE_ENV ? $Env:NODE_ENV : 'local')
$endpoint=$environment.storage.s3.config.endpoint
$containers=$environment.storage.s3.containers
Start-Sleep -Seconds 1
$containers |  ForEach-Object -Parallel {
    $name=$_[1]
    if ($using:verbose) {
        aws --debug --endpoint-url=$using:endpoint --no-sign-request s3 rb "s3://$name" --force
        aws --debug --endpoint-url=$using:endpoint --no-sign-request s3 mb "s3://$name" --region eu-west-1
    } else {
        aws --endpoint-url=$using:endpoint --no-sign-request s3 rb "s3://$name" --force
        aws --endpoint-url=$using:endpoint --no-sign-request s3 mb "s3://$name" --region eu-west-1
    }
}
