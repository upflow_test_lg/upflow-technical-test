import { ErrorRequestHandler } from 'express';

export const sendErrorStatusHandler: ErrorRequestHandler = (err, _req, res, next) => {
    if (err) {
        if (err.status) {
            res.sendStatus(err.status);
        } else {
            res.sendStatus(500);
        }
    }
    next();
};
