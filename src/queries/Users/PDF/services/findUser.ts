import { UserModel } from '../../../../models/User/UserModel';
import { UserNotFoundError } from '../errors';

export const findUser = async (userId: number) =>
    UserModel.findByPk(userId).then((user) => {
        if (!user) {
            throw new UserNotFoundError(userId);
        }
        return user;
    });
