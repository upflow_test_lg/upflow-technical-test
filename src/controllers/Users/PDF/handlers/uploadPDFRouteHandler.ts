import { RequestInputPostPDF } from '../types';
import { uploadPDFCommandAndNotifyUser } from '../../../../commands/Users/PDF';
import { RouteHandler } from '../../../types';
import { injectUserHandler } from '../../../utils';

const uploadPDFRouteHandler: RouteHandler<RequestInputPostPDF> = {
    method: 'post',
    path: '/users/:userId(\\d*)/PDF',
    inputSchema: {
        properties: {
            url: {
                type: 'string'
            }
        },
        additionalProperties: false
    },
    injectedHandlers: [injectUserHandler],
    routeHandler: (req, res) => {
        uploadPDFCommandAndNotifyUser({ ...req.body, user: (req as any)['injected'].user });
        res.status(200).send();
    }
};

export default uploadPDFRouteHandler;
