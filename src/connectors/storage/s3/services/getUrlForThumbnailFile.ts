import { getUrlToGetObject } from '../utils';

export const getUrlForThumbnailFile = (pdfThumbnailId: string) => getUrlToGetObject(pdfThumbnailId, 'ThumbnailUserPDF');
