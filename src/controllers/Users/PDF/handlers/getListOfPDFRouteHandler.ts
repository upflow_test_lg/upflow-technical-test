import { getListOfPDF } from '../../../../queries/Users/PDF/services/getListOfPDF';
import { RouteHandler } from '../../../types';
import { injectUserHandler } from '../../../utils';
import { RequestOutputGetPDF } from '../types';

const getListOfPDFRouteHandler: RouteHandler<never, RequestOutputGetPDF> = {
    method: 'get',
    path: '/users/:userId(\\d*)/PDF',
    outputSchema: {
        properties: {
            pdfs: {
                elements: {
                    properties: {
                        pdfS3Location: {
                            type: 'string'
                        },
                        thumbnailS3Location: {
                            type: 'string'
                        },
                        url: {
                            type: 'string'
                        }
                    },
                    additionalProperties: false
                }
            }
        },
        additionalProperties: false
    },
    injectedHandlers: [injectUserHandler],
    routeHandler: async (req, res, next) => {
        try {
            const pdfs = await getListOfPDF((req as any).injected.user.id);
            (res as any)['output'] = { pdfs };
            next();
        } catch (error) {
            next(error);
        }
    }
};

export default getListOfPDFRouteHandler;
