import { getUrlToGetObject } from '../utils';

export const getUrlForPDFFile = (pdfId: string) => getUrlToGetObject(pdfId, 'UserPDF');
