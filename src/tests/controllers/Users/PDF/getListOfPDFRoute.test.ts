import * as mocha from 'mocha';
import bluebird from 'bluebird';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import express from 'express';
import { getAvailablePortAndRetry } from '../../../utils/getAvailablePortAndRetry';
import { releasePort } from '../../../utils/releasePort';
import { runApplication } from '../../../../runApplication';
import { uploadPDFCommand } from '../../../../commands/Users/PDF';
import { UserModel } from '../../../../models/User/UserModel';
import { Server } from 'http';

chai.use(chaiHttp);

mocha.suite('Test Get List of PDF for an user', () => {
    let server: Server;
    const app = express();
    let users: UserModel[];
    mocha.suiteSetup('Create Users and PDFs for this test', async () => {
        server = runApplication(await getAvailablePortAndRetry(), app);
        users = await UserModel.bulkCreate([{ login: 'test@mailinator.com' }, { login: 'test2@mailinator.com' }]);
        try {
            await bluebird.Promise.map(
                [
                    'https://file-examples-com.github.io/uploads/2017/10/file-sample_150kB.pdf',
                    'https://file-examples-com.github.io/uploads/2017/10/file-example_PDF_500_kB.pdf',
                    'https://file-examples-com.github.io/uploads/2017/10/file-example_PDF_1MB.pdf'
                ],
                (url) => uploadPDFCommand({ user: users[0], url })
            );
        } catch (error) {
            console.log(error);
            throw error;
        }
    });
    mocha.suiteTeardown('Destroy users and pdfs', async () => {
        await bluebird.Promise.all([bluebird.Promise.map(users, (user) => user.destroy()), releasePort()]);
        server.close();
    });
    mocha.test('Get list of pdfs for user 1', async () => {
        const response = await chai.request(app).get(`/users/${users[0].id}/PDF`);
        expect(response.status).to.deep.equal(200);
        const pdfs: any[] = response.body.pdfs;
        console.log(pdfs);
        expect(pdfs.length).to.deep.equal(3);
        expect(
            pdfs.find((pdf) => pdf.url === 'https://file-examples-com.github.io/uploads/2017/10/file-sample_150kB.pdf')
        ).to.not.be.undefined;
        expect(
            pdfs.find(
                (pdf) => pdf.url === 'https://file-examples-com.github.io/uploads/2017/10/file-example_PDF_500_kB.pdf'
            )
        ).to.not.be.undefined;
        expect(
            pdfs.find(
                (pdf) => pdf.url === 'https://file-examples-com.github.io/uploads/2017/10/file-example_PDF_1MB.pdf'
            )
        ).to.not.be.undefined;
    });
    mocha.test('Get empty list of pdfs for user 2', async () => {
        const response = await chai.request(app).get(`/users/${users[1].id}/PDF`);
        expect(response.status).to.deep.equal(200);
        expect(response.body.pdfs).deep.equal([]);
    });
    mocha.test('Get list of pdfs for inexistant user', async () => {
        const response = await chai.request(app).get(`/users/${Math.max(users[1].id, users[0].id) + 1}/PDF`);
        expect(response.status).to.deep.equal(404);
    });
});
