import * as mocha from 'mocha';
import bluebird from 'bluebird';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import express from 'express';
import io from 'socket.io-client';
import { getAvailablePortAndRetry } from '../../../utils/getAvailablePortAndRetry';
import { releasePort } from '../../../utils/releasePort';
import { runApplication } from '../../../../runApplication';
import { UserModel } from '../../../../models/User/UserModel';
import { waitUntil } from 'async-wait-until';
import { Server } from 'http';

chai.use(chaiHttp);

mocha.suite('Test Upload PDF', () => {
    let server: Server;
    let port: number;
    const app = express();
    let user: UserModel;
    mocha.suiteSetup('Create User for this test', async () => {
        port = await getAvailablePortAndRetry();
        server = runApplication(port, app);
        user = await UserModel.create({ login: 'test@mailinator.com' });
    });
    mocha.suiteTeardown('Destroy user', async () => {
        await bluebird.Promise.all([user.destroy(), releasePort()]);
        server.close();
    });
    mocha.test('Successfully upload PDF with thumbnail to S3', async () => {
        const socket = io(`http://localhost:${port}`, {
            query: {
                userId: String(user.id)
            },
            path: '/websockets'
        });
        socket.connect();
        let uploadProcessingFinished = false;
        let receivedS3PDFFileLocation = '';
        let receivedS3ThumbnailFileLocation = '';
        socket.on('Successful Upload', (s3PDFFileLocation, s3ThumbnailFileLocation) => {
            receivedS3PDFFileLocation = s3PDFFileLocation;
            receivedS3ThumbnailFileLocation = s3ThumbnailFileLocation;
            uploadProcessingFinished = true;
        });
        const response = await chai
            .request(app)
            .post(`/users/${user.id}/PDF`)
            .send({ url: 'http://www.africau.edu/images/default/sample.pdf' });
        expect(response.status).to.deep.equal(200);
        await waitUntil(() => uploadProcessingFinished, {
            timeout: 2000,
            intervalBetweenAttempts: 10
        });
        expect(uploadProcessingFinished).to.be.true;
        console.log(receivedS3PDFFileLocation);
        console.log(receivedS3ThumbnailFileLocation);
        expect(receivedS3PDFFileLocation.length).to.be.greaterThan(0);
        expect(receivedS3ThumbnailFileLocation.length).to.be.greaterThan(0);
        socket.disconnect();
    });
    mocha.test('Error user not found', async () => {
        const response = await chai
            .request(app)
            .post(`/users/${user.id + 1}/PDF`)
            .send({ url: 'http://www.africau.edu/images/default/sample.pdf' });
        expect(response.status).to.deep.equal(404);
    });
    mocha.test('Error duplicate', async () => {
        const socket = io(`http://localhost:${port}`, {
            query: {
                userId: String(user.id)
            },
            path: '/websockets'
        });
        socket.connect();
        let uploadProcessingFinished = false;
        let errorCaught = null;
        socket.on('Error Upload', (error) => {
            uploadProcessingFinished = true;
            errorCaught = error;
        });
        const response = await chai
            .request(app)
            .post(`/users/${user.id}/PDF`)
            .send({ url: 'http://www.africau.edu/images/default/sample.pdf' });
        expect(response.status).to.deep.equal(200);
        await waitUntil(() => uploadProcessingFinished, {
            timeout: 2000,
            intervalBetweenAttempts: 10
        });
        expect(uploadProcessingFinished).to.be.true;
        expect(errorCaught).to.deep.include({
            name: 'SequelizeUniqueConstraintError',
            fields: ['userId', 'url']
        });
        socket.disconnect();
    });
    mocha.test('Error not a pdf', async () => {
        const socket = io(`http://localhost:${port}`, {
            query: {
                userId: String(user.id)
            },
            path: '/websockets'
        });
        socket.connect();
        let uploadProcessingFinished = false;
        let errorCaught = null;
        socket.on('Error Upload', (error) => {
            uploadProcessingFinished = true;
            errorCaught = error;
        });
        const response = await chai
            .request(app)
            .post(`/users/${user.id}/PDF`)
            .send({ url: 'https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg' });
        expect(response.status).to.deep.equal(200);
        await waitUntil(() => uploadProcessingFinished, {
            timeout: 2000,
            intervalBetweenAttempts: 10
        });
        expect(uploadProcessingFinished).to.be.true;
        expect(errorCaught).to.deep.include({
            name: 'ObjectNotPDFError',
            userId: user.id,
            url: 'https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg',
            contentType: 'image/jpeg'
        });
        socket.disconnect();
    });
    mocha.test('Error inexistant URL', async () => {
        const socket = io(`http://localhost:${port}`, {
            query: {
                userId: String(user.id)
            },
            path: '/websockets'
        });
        socket.connect();
        let uploadProcessingFinished = false;
        let errorCaught = null;
        socket.on('Error Upload', (error) => {
            uploadProcessingFinished = true;
            errorCaught = error;
        });
        const response = await chai
            .request(app)
            .post(`/users/${user.id}/PDF`)
            .send({ url: 'https://inexistant16554612.com/test.pdf' });
        expect(response.status).to.deep.equal(200);
        await waitUntil(() => uploadProcessingFinished, {
            timeout: 2000,
            intervalBetweenAttempts: 10
        });
        expect(uploadProcessingFinished).to.be.true;
        expect(errorCaught).to.deep.include({
            message: 'getaddrinfo ENOTFOUND inexistant16554612.com',
            name: 'Error',
            code: 'ENOTFOUND'
        });
        socket.disconnect();
    });
});
