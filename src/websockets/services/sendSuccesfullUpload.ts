import io from 'socket.io';
import { getLogger } from '../../connectors/logger';

export const sendSuccessfullUpload = (userId: number, s3PDFFileLocation: string, s3ThumbnailFileLocation: string) => {
    getLogger().info('Notify Room With Succesful Upload', { userId });
    ((globalThis as any).wsNotificationUploadImage as io.Server)
        .in(String(userId))
        .emit('Successful Upload', s3PDFFileLocation, s3ThumbnailFileLocation);
};
