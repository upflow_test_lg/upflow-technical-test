import config from '../config/config.json';

export const getConfigFromEnv = () => {
    return (config as any)[process.env['NODE_ENV'] || 'local'];
};
