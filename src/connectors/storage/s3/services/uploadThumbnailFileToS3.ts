import { File } from '../types';
import { uploadFileInBucket } from '../utils';

export const uploadThumbnailFileToS3 = (params: File) =>
    uploadFileInBucket({
        ...params,
        bucketname: 'ThumbnailUserPDF'
    });
