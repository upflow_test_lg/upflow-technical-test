import { Sequelize } from 'sequelize-typescript';

export const shutdown = () => ((globalThis as any).sequelize as Sequelize)?.close();
