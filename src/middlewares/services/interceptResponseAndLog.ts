import { RequestHandler } from 'express';
import { getLogger } from '../../connectors/logger';
import _ from 'lodash';

export const interceptResponseAndLog: RequestHandler = (req, res, next) => {
    const oldSend = res.send;
    res.send = (data) => {
        getLogger().info(
            'Results',
            { data, statusCode: res.statusCode },
            _.pick(req, ['origin', 'method', 'path', 'hostname', 'port'])
        );
        res.send = oldSend;
        return res.send(data);
    };
    next();
};
