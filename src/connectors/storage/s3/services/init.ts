import { S3, S3ClientConfig } from '@aws-sdk/client-s3';

export const init = (config: S3ClientConfig, containers: [string, string][]) => {
    (globalThis as any).s3Client = new S3(config);
    (globalThis as any).s3Containers = new Map<string, string>(containers);
};
