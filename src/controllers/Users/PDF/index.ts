import getListOfPDFRouteHandler from './handlers/getListOfPDFRouteHandler';
import uploadPDFRouteHandler from './handlers/uploadPDFRouteHandler';
import { Controller } from '../../types';
import { getRouterFromHandler } from '../../utils';
import { Router } from 'express';

const PDFController: Controller = {
    router: getRouterFromHandler(getRouterFromHandler(Router(), uploadPDFRouteHandler), getListOfPDFRouteHandler)
};

export default PDFController;
