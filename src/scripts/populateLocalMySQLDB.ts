import bluebird from 'bluebird';
import config from '../config/config.json';
import { ApplicationPort } from '../tests/utils/ApplicationPort';
import { Command } from 'commander';
import { Sequelize, SequelizeOptions } from 'sequelize-typescript';
import { UserModel, UserPDFModel } from '../models/User/UserModel';

const createDefaultUsers = async () => {
    const { databases } = config[process.env['NODE_ENV'] || 'local'];
    if (databases.sql.config.storage === ':memory:') {
        return;
    }
    const sequelize = new Sequelize(databases.sql.config as SequelizeOptions);
    sequelize.addModels([UserModel, UserPDFModel]);
    await sequelize.sync();
    const { testUsers } = databases.sql;
    if (!testUsers) {
        return;
    }
    console.log(testUsers);
    return bluebird.Promise.map(testUsers, (testUser: any) =>
        UserModel.findOrCreate({
            where: { id: testUser.id },
            defaults: testUser
        })
    );
};

const createPorts = async () => {
    const { databases } = config[process.env['NODE_ENV'] || 'test'];
    const sequelize = new Sequelize(databases.portManagementSql.config as SequelizeOptions);
    sequelize.addModels([ApplicationPort]);
    await sequelize.sync();
    return ApplicationPort.bulkCreate(
        [
            { port: 8080 },
            { port: 8081 },
            { port: 8082 },
            { port: 8083 },
            { port: 8084 },
            { port: 8085 },
            { port: 8086 },
            { port: 8087 },
            { port: 8088 },
            { port: 8089 },
            { port: 8090 },
            { port: 8091 },
            { port: 8092 },
            { port: 8093 },
            { port: 8094 },
            { port: 8095 },
            { port: 8096 },
            { port: 8097 },
            { port: 8098 },
            { port: 8099 }
        ].filter((value) => value.port !== (Number(process.env['PORT']) || 8080))
    );
};

const program = new Command();

program
    .description('populate MySQL DB')
    .parseAsync(process.argv)
    .then(async () => {
        await bluebird.Promise.all([createDefaultUsers(), createPorts()]);
        process.exit(0);
    });
