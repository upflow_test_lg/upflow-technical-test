export interface File {
    Key: string;
    Body: Buffer;
}
