import bluebird from 'bluebird';
import { initConnectors } from '../utils/initConnectors';
import { initPortManagement } from './utils/initPortManagement';
import { shutdownConnectors } from '../utils/shutdownConnectors';
import { shutdownPortManagement } from './utils/shutdownPortManagement';

exports.mochaHooks = {
    beforeAll: async () => {
        initConnectors();
        initPortManagement();
        await bluebird.Promise.all([
            (globalThis as any).sequelize.sync(),
            (globalThis as any).sequelizePortManagement.sync()
        ]);
    },
    afterAll: async () => {
        await bluebird.Promise.all([shutdownPortManagement(), shutdownConnectors()]);
    }
};
