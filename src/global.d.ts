import io from 'socket.io';
import { Logger } from 'winston';
import { S3 } from '@aws-sdk/client-s3';
import { Sequelize } from 'sequelize-typescript';

declare let wsNotificationUploadImage: io.Server;
declare let s3Client: S3;
declare let isShutdown: boolean;
declare let isInitialized: boolean;
declare let s3Containers: Map<string, string>;
declare let sequelize: Sequelize;
declare let sequelizePortManagement: Sequelize;
declare let winstonLogger: Logger;
